import { StyleSheet } from 'react-native';

export default StyleSheet.create({
 container: {
    backgroundColor: '#fff',
    position: 'relative',
    width: "100%",
    height:"100%",
  },
  headerTops: {
      width: "100%",       
      backgroundColor: '#5f021f',
      top:20,
      color:'white',
      position: 'absolute',
      textAlign:'center',
      padding:10,
      zIndex:99999,
  },
  imageCenter: {
      width: "100%",
      textAlign:'center',
      padding:10,
      alignItems:'center',
      display:'flex',
      justifyContent:'center',
  },

  FlexCenter:{
      alignItems:'center',
      display:'flex',
      justifyContent:'center',
    },

    LogininputIcon:{
      alignItems:'center',
      display:'flex',
      justifyContent:'center',
      width: "15%"
    },

  FooterImage:{
      height:20,
    width:20,
  },
  FooterImageStaff:{
    height:20,
    width:20,
},
  FooterBottom: {
      width: "100%",
      bottom:0,
      color:'white',
      position: 'absolute',
      textAlign:'center',
      backgroundColor: '#5f021f',
  },
  ColSet50:{
    width: "50%",
    height: 50,
    backgroundColor: '#5f021f',
    alignItems:'center',
      display:'flex',
      justifyContent:'center',
  },
   containerCenter: {
    backgroundColor: '#fff',
    textAlign:'center',
    position: 'absolute',
	top:5,
    bottom:60,
    width: "100%",
    height:"100%",
  },
  imageLogo:{
    height:60,
  },
  inputSet:{
    height: 40,
    width:"90%",
    padding:10,    
    fontSize:18,
  },
  buttonRed:{
     backgroundColor: 'red',
     width:"100%",
  },
  input:{
      backgroundColor: 'white',
      width:"90%",
      padding:10,
    },
    text:{
        padding:10,
        fontSize:20

    },

    ColSet10:{
      width: "10%"
    },
    ColSet15:{
      width: "15%",
	 alignItems:'flex-end'
    },
    ColSet20:{
      width: "20%"
    },
    ColSet30:{
      width: "30%"
    },
	 ColSet33:{
      width: "33%"
    },
    ColSet40:{
      width: "40%"
    },
    ColSet50:{
      width: "50%"
    },
    ColSet55:{
      width: "55%"
    },
    ColSet60:{
      width: "60%"
    },
    ColSet70:{
      width: "70%"
    },
    ColSet80:{
      width: "80%"
    },
    ColSet85:{
      width: "85%"
    },
    ColSet90:{
      width: "90%"
    },

    iconSize:{
       height:20,
        width:20,
    },

    iconSizeLock:{
      height:10,
       width:10,
   },

    iconSizecall:{
      height:28,
        width:28,
    },
    iconSizemail:{
      height:28,
        width:28,
		
    },
    CallBg:{
      backgroundColor:'green',
      width: "15%"
    },

	leftMenuContainer:{
		marginTop:10
		
	},
	
	sectionHeadingStyle:{
		height:50,
		backgroundColor:'#5f021f',
		marginTop:15,
		color:'#ffff',
		lineHeight:50,
		fontSize:18
		
	},
	navSectionStyle:{
		flex:1,
		flexDirection:'row',
		height:50
		
		
	},
	navMenuIcon:{
		paddingTop:10,
		marginTop:15,
		marginLeft:5
		
	},
	navItemStyle:{
		paddingLeft:8,
		paddingTop:15,
		lineHeight:35
	},
	footerMenuBtn:{
		marginLeft:"1%"
		
	},
	fontSizeText:{
      color:'black',
      fontSize:20
    },

    textRotat: {
        transform: [{ rotate: '90deg'}],
        fontSize:12,
        textAlign:'center',
        backgroundColor:'grey',
    },

    boxCenterDiv: {
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    textAlignDiv: {
        flex:1,
        flexDirection:'row',
        alignItems:'center',
    },

    BorderBox:{
      borderColor: 'gray',
      borderWidth: 1,
        margin:2,
        textAlign:'center',

    },

    tableGrid18:{
      width: "18.8%",
      textAlign:'center',
      borderColor: 'gray',
      borderRightWidth: 1,
    },

    tableGrid18Yellow:{
      width: "18.8%",
      textAlign:'center',
      borderColor: 'gray',
      borderRightWidth: 1,
      backgroundColor: '#fff1ce',
      paddingBottom:4,
      paddingTop:4,
    },

    tableGrid18Greys:{
      width: "18.8%",
      textAlign:'center',
      borderColor: 'gray',
      borderRightWidth: 1,
      backgroundColor: '#fefffc',
      paddingBottom:4,
      paddingTop:4,
    },
    
    tableGrid10:{
      width: "6%",
      textAlign:'center',
      borderColor: 'gray',
      borderRightWidth: 1,
      backgroundColor: '#e1e1e1',
    },

    tableText:{
      color:'black',
      fontSize:13,
      textAlign:'center',
    },
    gridTitle:{
      color:'black',
      fontSize:18,
      textAlign:'center',
      paddingBottom:6,
      paddingTop:6,
      backgroundColor: '#e1e1e1',
    },

    bgnumber:{
      backgroundColor: '#911610',
      color:'white',
      paddingLeft:6,
      paddingRight:6,
      borderRadius:20

    },
	secretCol:{
		 height: 30,
		 width: "95%", 
		 borderColor: 'gray', 
		 borderWidth: 1,
		 textAlign:'center'
		
	},
	staffMenuRightArrow:{
		flex:1,
		 alignItems:'flex-end',
		width:50,
		height:30,
		display:'flex'
		
		
		
	},
	rightArrowIconSize:{
		marginTop:4
		
	},
	staffMenuName:{
		textAlign:'center',
		color:'black' ,
		paddingTop:3
		
	}

    
});
import I18n from 'react-native-i18n'
import { AsyncStorage } from 'react-native'
import { SET_LOCALE,GET_LOCALE,
  SET_TRANSLATIONS ,
  ADD_STUDENT,
  GET_STUDENT_LOCAL
} from '../actions/actions'

const initialState = {
  locale: 'ar',
  version: null,
  toggle: false,
  shoolId:'',
  password:'',
  students:[]
}

export default function reducer(state =initialState, action = {}) {
  const { payload } = action

  switch (action.type) {
	  
    case SET_LOCALE:
	//console.log("hai",payload.locale);
      I18n.locale = payload.locale
	AsyncStorage.setItem('lang', payload.locale);
      return {
        ...state,
        locale:payload.locale,
		isRTL: payload.locale=='ar'? true: false
      }
	 case GET_LOCALE:
		console.log("Locale in reducer",AsyncStorage.getItem('lang'))
      return {
        ...state,
        locale:AsyncStorage.getItem('lang'),
		    isRTL: AsyncStorage.getItem('lang')=='ar'? true: false
      } 
	
    case SET_TRANSLATIONS:
      const { translations } = payload
      I18n.translations = translations

      return {
        ...state,
        version: translations._version || state.version,
        toggle: !state.toggle,
      }

    case GET_STUDENT_LOCAL:
    console.log("in reducer  GET_STUDENT_LOCAL")
      console.log(payload)
      return {
        ...state,
        students: JSON.parse(payload)
      }

    case ADD_STUDENT:
      console.log("in reducer add student")
      let students = [...state.students,...payload];
      // students.push(payload)
      AsyncStorage.setItem('students', JSON.stringify(students));
      return {
        ...state,
        students:students,
      }

    default:
      return state
  }
}
import React from 'react';
import {
    Text, View, TextInput, Image
} from 'react-native';
import Mail from '../../imagesicon/message_14.png';
import Call from '../../imagesicon/loginscreen_12.png';
import I18n from '../../i18n.js';
import StyleSheetFactory from '../../styles/Ar_loginStyles.js';
class StudentListComp extends React.Component {
    constructor(props) {
        super(props);
        let isRTL = this.props.locale == "ar" ? true : false;
        this.state = { isRTL: isRTL };
        I18n.locale = this.state.locale; 
    }
    render() {
        if (this.state.isRTL) {
            styles = StyleSheetFactory.getSheet(this.state.isRTL);
        }
        styles = StyleSheetFactory.getSheet(this.state.isRTL);
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', backgroundColor: '#f2f2f2', padding: 6, marginBottom: 10 }}>
                <View style={styles.ColSet10}>
                    <TextInput style={styles.secretCol}
                        onChangeText={(text) => this.setState({ text })}
                        placeholder="" underlineColorAndroid="transparent" />
                </View>
                <View style={styles.ColSet60}>
                    <Text style={{ textAlign: this.state.isRTL ? 'right' : 'left', color: 'black', paddingLeft: this.state.isRTL ? 0 : 8, paddingRight: this.state.isRTL ? 8 : 0 }}>
                    {this.props.student.st_name}</Text>
                </View>
                <View style={styles.ColSet15}>
                    <Image style={styles.iconSizecall} source={Call} />
                </View>

                <View style={styles.ColSet15}>
                    <Image style={styles.iconSizemail} source={Mail} />
                </View>
            </View>
        );
    }
}

export default StudentListComp
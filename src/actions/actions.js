import axios from 'axios'
import { AsyncStorage,ToastAndroid } from 'react-native'

export const SET_LOCALE = 'SET_LOCALE'
export const GET_LOCALE = 'GET_LOCALE'
export const SET_TRANSLATIONS = 'SET_TRANSLATIONS'
export const ADD_STUDENT = 'ADD_STUDENT'
export const GET_STUDENT_LOCAL = 'GET_STUDENT_LOCAL'

export const setLocale = locale => ({
 
  type: SET_LOCALE,
  payload: { locale },
})

let form = new FormData();
form.append("token_secure", "$2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ");

const fetchMyChilds = {
  url : 'https://www.nenewe.com/fetch-my-childs/',
  body:{
    schoolId:'97Uu',
    st_password:'586',
    token_secure:'$2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ'
  }
}

export const  receiveStudent = (students) => ({
    type: ADD_STUDENT,
    payload:students
})


export function addStudent(schoolId,password){
  console.log("In add student")
  console.log(schoolId)
  form.append("schoolId", schoolId);
  form.append("st_password", password);
  return async function (dispatch) {
    // dispatch(fetchingStudent())
      try {
        let student = await axios.post(fetchMyChilds.url,form)
       
        console.log(student.data)
        if(student.data.length > 0 ){
          ToastAndroid.show('Added New Student !', ToastAndroid.SHORT);
        } else {
          ToastAndroid.show('No Student Found!', ToastAndroid.SHORT);
        }
        
        dispatch(receiveStudent(student.data))
      }
      catch(err){
        console.log(err)

        // dispatch(loadQuotesError())
      }
  }
}

export const getLocale =() => ({

  type: GET_LOCALE

})

export function getStudentLocal(){
  console.log("==============Action get student local")
  return async function (dispatch) {
    // dispatch(fetchingStudent())
      try {
        let student = await AsyncStorage.getItem('students');
        console.log("retrieving local data for students")
        console.log(student)
        if (!student) {
          student = []
        }
        dispatch(receiveStudentLocal(student))
      }
      catch(err){
        console.log(err)

        // dispatch(loadQuotesError())
      }
  }
}

export const receiveStudentLocal =(studentLocal) => ({

  type: GET_STUDENT_LOCAL,
  payload:studentLocal

})

export const setTranslations = translations => ({
  type: SET_TRANSLATIONS,
  payload: { translations },
})
import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,Switch,TouchableOpacity} from 'react-native';
import styles from '../styles/LoginStyles';
import Delete from '../imagesicon/setting_11.png';
export default class SoundSetting extends React.Component {
   
   constructor(props) {
      super(props);
      this.state = { SwitchValue: false };
    }

  render() {
    return (
      <View style={styles.container} >
        <View style={styles.headerTops} >
            <View style={{flex: 0, flexDirection: 'row'}}>
                <View style={styles.ColSet20}>
                    <View style = {styles.boxCenterDiv}>
                         <TouchableOpacity onPress={this.showCalculation}>
                           
                        </TouchableOpacity>
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                    <View style = {styles.textAlignDiv} >
                   <Text style={{color:'white',fontSize:18}}>Time Table</Text>
                    </View>
                 </View> 
             </View>
        </View>
          
          <View style={styles.containerCenter}>
            <View style={{flex: 0, flexDirection: 'row'}}>
                <View style={styles.ColSet20}>
                    <View style = {styles.boxCenterDiv}>
                        <View style = {styles.bgnumber}>
                        <Text style={{color:'white',fontSize:14}}> 2/2</Text>
                       </View> 
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                  <Text style={{color:'#911610',fontSize:30,paddingBottom:6,paddingTop:6}}>School Name</Text>
                </View> 
             </View>
            
             <View style={{flex: 0, flexDirection: 'row',borderWidth:1,borderBottomColor: 'black',}}>
                <View style={styles.tableGrid10}>
                    <View style = {styles.textAlignDiv} >
                    <Text style={{color:'black',fontSize:12}}></Text>
                    </View>
                 </View>                 
                 <View style={styles.tableGrid18}>
                    
                         <Text style={styles.gridTitle}>Sun</Text>
                     
                 </View>
                 <View style={styles.tableGrid18}>
                         <Text style={styles.gridTitle}>Mon</Text>
                    
                 </View>
                 <View style={styles.tableGrid18}>
                         <Text style={styles.gridTitle}>Tue</Text>
                 </View>
                 <View style={styles.tableGrid18}>
                         <Text style={styles.gridTitle}>Wed</Text>
                 </View>
                 <View style={styles.tableGrid18}>
                         <Text style={styles.gridTitle}>Thu</Text>
                 </View>
             </View>
             <View style={{flex: 0, flexDirection: 'row',borderBottomWidth:1,borderBottomColor: 'black'}}>
                <View style={styles.tableGrid10}>
                    <View style = {styles.boxCenterDiv} >
                    <Text style={{color:'black',fontSize:12}}>1</Text>
                    </View>
                 </View>                 
                 <View style={styles.tableGrid18Yellow}>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec3</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec5</Text>
                 </View>
                 <View style={styles.tableGrid18Greys}>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec3</Text>
                 </View>
                 <View style={styles.tableGrid18Yellow}>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec3</Text>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec5</Text>
                 </View>
                 <View style={styles.tableGrid18Greys}>
                         <Text style={styles.tableText}>lec3</Text>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec1</Text>
                 </View>
                 <View style={styles.tableGrid18Yellow}>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec3</Text>
                 </View>
             </View>
             <View style={{flex: 0, flexDirection: 'row',borderBottomWidth:1,borderBottomColor: 'black'}}>
                <View style={styles.tableGrid10}>
                    <View style = {styles.boxCenterDiv} >
                    <Text style={{color:'black',fontSize:12}}>2</Text>
                    </View>
                 </View>                 
                 <View style={styles.tableGrid18Greys}>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec3</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec5</Text>
                 </View>
                 <View style={styles.tableGrid18Yellow}>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec3</Text>
                 </View>
                 <View style={styles.tableGrid18Greys}>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec3</Text>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec5</Text>
                 </View>
                 <View style={styles.tableGrid18Yellow}>
                         <Text style={styles.tableText}>lec3</Text>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec1</Text>
                 </View>
                 <View style={styles.tableGrid18Greys}>
                         <Text style={styles.tableText}>lec2</Text>
                         <Text style={styles.tableText}>lec1</Text>
                         <Text style={styles.tableText}>lec4</Text>
                         <Text style={styles.tableText}>lec5</Text>
                         <Text style={styles.tableText}>lec3</Text>
                 </View>
             </View>
        </View>
      </View>
    );
  }
}


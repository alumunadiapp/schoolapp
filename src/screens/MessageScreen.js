import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,CheckBox,TouchableHighlight} from 'react-native';
import styles from '../styles/LoginStyles';
import Mail from '../imagesicon/message_14.png';
import I18n  from '../i18n.js';
import { connect } from 'react-redux';
 class MessageScreen extends React.Component {
	 constructor(props) {
      super(props);
	 //console.log("dsdsdsdsd",I18nManager.isRTL);
	let isRTL= this.props.locale.locale=="ar" ? true : false ;
	this.state = { isRTL: isRTL,
				  checked:true

	};
	
	 I18n.locale = this.state.locale;
    }

	 componentDidMount() {
            this.props.navigation.setParams({});
               
        }	
	static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('School App'),
           
            };
        };
   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }

  render() {
	     const { navigate } = this.props.navigation;
    return (
      <View style={styles.container} >
                
          <View style={styles.containerCenter}>
             <View style={{flex: 0, flexDirection: 'row',backgroundColor:'#f9f9f9',padding:6,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
			   <TouchableHighlight onPress={()=>navigate('MessageDetails')}>
                <View style={styles.ColSet20}>
                    <Image style={{height:30,width:30,}} source={Mail}/>
                 </View>
				</TouchableHighlight>
                <View style={styles.ColSet70}>
                    <Text style={{color:'black',paddingLeft:10}}>Appex Public School</Text>
                  <Text style={{color:'black',paddingLeft:10}}>Fdddd <Text style={{color:'grey',fontSize:10}}>7/9/19 8:20 AM</Text></Text>
                 </View>                 
                 <View style={styles.ColSet10}>
                 <CheckBox/>
                 </View>
				
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',backgroundColor:'#f9f9f9',padding:6,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
			  <TouchableHighlight onPress={()=>navigate('MessageDetails')}>
             <View style={styles.ColSet20}>
                    <Image style={{height:30,width:30,}} source={Mail}/>
                 </View>
				 </TouchableHighlight>
                <View style={styles.ColSet70}>
                  <Text style={{color:'black',paddingLeft:10}}>Appex Public School</Text>
                  <Text style={{color:'black',paddingLeft:10}}>Fdddd</Text>
                 </View>                 
                 <View style={styles.ColSet10}>
                 <CheckBox/>
                 </View>
				
             </View>
             <View style={{flex: 0, flexDirection: 'row',backgroundColor:'#f9f9f9',padding:6,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
			  <TouchableHighlight onPress={()=>navigate('MessageDetails')}>
             <View style={styles.ColSet20}>
                    <Image style={{height:30,width:30,}} source={Mail}/>
                 </View>
				 </TouchableHighlight>
                <View style={styles.ColSet70}>
                  <Text style={{color:'black',paddingLeft:10}}>Appex Public School</Text>
                  <Text style={{color:'black',paddingLeft:10}}>Fdddd <Text style={{color:'grey',fontSize:10}}>7/9/19 8:20 AM</Text></Text>
                 </View>                 
                 <View style={styles.ColSet10}>
                 <CheckBox/>
                 </View>
				
             </View>
             <View style={{flex: 0, flexDirection: 'row',backgroundColor:'#f9f9f9',padding:6,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
			  <TouchableHighlight onPress={()=>navigate('MessageDetails')}>
             <View style={styles.ColSet20}>
                    <Image style={{height:30,width:30,}} source={Mail}/>
                 </View>
				 </TouchableHighlight>
                <View style={styles.ColSet70}>
                  <Text style={{color:'black',paddingLeft:10}}>Appex Public School</Text>
                  <Text style={{color:'black',paddingLeft:10}}>Fdddd <Text style={{color:'grey',fontSize:10}}>7/9/19 8:20 AM</Text></Text>
                 </View>                 
                 <View style={styles.ColSet10}>
                 <CheckBox/>
                 </View>
				
             </View>
        </View>

      </View>
    );
  }
}
const mapStateToProps = (state) => {
	
  return {
   locale: state.locale,

  }
}

 

export default connect(mapStateToProps)( MessageScreen )

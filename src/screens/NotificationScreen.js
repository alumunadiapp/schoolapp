import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,CheckBox} from 'react-native';
//import styles from '../styles/LoginStyles';
import { connect } from 'react-redux';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
import { RadioButton } from 'react-native-paper';
import I18n  from '../i18n.js';
class NotificationScreen extends React.Component {
    state = {
    checked: 'always',
  };
  static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('Notification Settings'),
           
            };
    };
  render() {
	  I18n.locale = this.props.locale.locale;
	  styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
	   const { checked } = this.state;
    return (
      <View style={styles.container} >
        
          <View style={styles.containerCenter}>
             <View style={{flex: 0, flexDirection: this.props.locale.isRTL ? 'row-reverse' :'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                
                <View style={styles.ColSet90}>
                    <View style={styles.textAlignDiv}>
                    <Text style={{color:'black',fontSize:18}}>{ I18n.t('Never Appear')}</Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet10}>
                   <RadioButton
					  value="never"
					  status={checked === 'never' ? 'checked' : 'unchecked'}
					  onPress={() => { this.setState({ checked: 'never' }); }}
					/>
                 </View>
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
             
                <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                <Text style={{color:'black',fontSize:18}}>{ I18n.t('When the screen is open')}</Text>
                </View>
                 </View>                 
                 <View style={styles.ColSet10}>
                   <RadioButton
					  value="open"
					  status={checked === 'open' ? 'checked' : 'unchecked'}
					  onPress={() => { this.setState({ checked: 'open' }); }}
					/>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                
                <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                    <Text style={{color:'black',fontSize:18}}>{ I18n.t('When the screen is closed')}</Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet10}>

                   <RadioButton
					  value="close"
					  status={checked === 'close' ? 'checked' : 'unchecked'}
					  onPress={() => { this.setState({ checked: 'close' }); }}
					/>
                 </View>
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
             
                <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                <Text style={{color:'black',fontSize:18}}>{ I18n.t('Always Appear')}</Text>
                </View>
                 </View>                 
                 <View style={styles.ColSet10}>
                      <RadioButton
					  value="always"
					  status={checked === 'always' ? 'checked' : 'unchecked'}
					  onPress={() => { this.setState({ checked: 'always' }); }}
					/>
                 </View>
             </View>
             <View  style={{margin:10,backgroundColor: '#911610'}}>
                <Button color="#5f021f" title={ I18n.t('SAVE')} onPress={this.addUser}/>
              </View>
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
	
	return {
   locale: state.locale,
    isRTL: state.isRTL,

  }
}
 
  const mapDispatchToProps = dispatch => ({
    setLocale: (locale) => dispatch(setLocale(locale))
  })
 

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen)
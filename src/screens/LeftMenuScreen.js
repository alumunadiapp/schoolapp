import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,ScrollView,I18nManager} from 'react-native';
//import styles from '../styles/LoginStyles';
import Logo from '../images/newstudent.png';
import Mail from '../images/mail.png';
import Call from '../images/phone-green.png';
import Parent from '../images/Parent-icon.png';
import Staff from '../images/man-icon-white.png';
import Degree from '../images/graduation.png';
import Lock from '../images/lock.png';
import LanguageIcon from '../imagesicon/setting_03.png';
import NotifiactionIcon from '../imagesicon/setting_06.png';
import SoundIcon from '../imagesicon/setting_08.png';
import SoundIconRight from '../imagesicon/right_sound.png';
import DeleteIcon from '../imagesicon/setting_11.png';
import ShareIcon from '../imagesicon/setting_14.png';
import UpdateIcon from '../imagesicon/setting_17.png';
import LogoutIcon from '../imagesicon/setting_19.png';
import { getLocale } from '../actions/actions';
import I18n  from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { NavigationActions } from 'react-navigation';
class LeftMenuScreen extends React.Component {

	 constructor(props) {
		super(props);
		let isRTL= this.props.locale.locale=="ar" ? true : false ;
		
		this.state = { isRTL: isRTL };
		I18n.locale = this.state.locale;
		 
    }
	  componentDidMount() {
		  getLocale();
		  // this.props.store.dispatch(sendMessage(message))
		  console.log("page load",  this.props.store);
		//this.props.dispatch(getLocale());
		
   // this.props.actions.getLocale();
  }
	
   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }
	
	navigateToScreen = (route) => () => {
		console.log("dsdsds",route);
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }


 render () {
	 console.log("RTL", this.state.isRTL);
	  console.log("locakle",  this.state.locale);
	 styles = StyleSheetFactory.getSheet(this.state.isRTL);
	
    return (
      <View style={styles.leftMenuContainer}>
        <ScrollView>
          <View>
            <Text style={styles.sectionHeadingStyle}>
              {I18n.t('Settings')}
            </Text>
		
			
            <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={LanguageIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LanguageSetting')}>
				 {I18n.t('Language Settings')}
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={NotifiactionIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('NotificationSetting')}>
				
				 {I18n.t('Notification Settings')}
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={ this.state.isRTL ? SoundIconRight : SoundIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('SoundSetings')}>
				
				 {I18n.t('Sound Settings')}
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={DeleteIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('DeleteStudent')}>
				
				 {I18n.t('Delete Student')}
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={ShareIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Share App')}>
			 {I18n.t('Share App')}
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={UpdateIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LanguageSetings')}>
				 {I18n.t('Update')}
              </Text>
            </View>
			<View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={LogoutIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LanguageSetings')}>
				 {I18n.t('Logout')}
              </Text>
            </View>
			
          </View>
       
        </ScrollView>
       
      </View>
    );
  }
}

LeftMenuScreen.propTypes = {
  navigation: PropTypes.object
};

const mapStateToProps = (state) => {
	
  return {
   locale: state.locale,
    isRTL: state.isRTL,

  }
}

  const mapDispatchToProps = dispatch => ({
    getLocale: () => dispatch(getLocale())
  })

export default connect(mapStateToProps,mapDispatchToProps)(LeftMenuScreen)

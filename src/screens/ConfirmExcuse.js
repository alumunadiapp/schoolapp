import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,CheckBox} from 'react-native';
//import styles from '../styles/LoginStyles';
import Person from '../imagesicon/home3_08.png';
import Apply from '../imagesicon/home3_09.png';
import Confirm from '../imagesicon/home3_10.png';
import cross from '../imagesicon/staff2_05.png';
import confirmStaffIcon from '../imagesicon/confirmStaff.png';
import confirmMsgIcon from '../imagesicon/msg_icon.png';
import rightTick from '../imagesicon/right_tick.png';
import I18n  from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
//import styles from '../styles/LoginStyles';
import { connect } from 'react-redux';
class ConfirmExcuse extends React.Component {
	
	 constructor(props) {
      super(props);
	 //console.log("dsdsdsdsd",I18nManager.isRTL);
	let isRTL= this.props.locale.locale=="ar" ? true : false ;
	this.state = { isRTL: isRTL };
	 I18n.locale = this.state.locale;
    }

	 componentDidMount() {
            this.props.navigation.setParams({});
               
        }	
	static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('School App'),
           
            };
        };
   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }

  render() {
	  if(this.state.isRTL){
			styles = StyleSheetFactory.getSheet(this.state.isRTL);
		}
		styles = StyleSheetFactory.getSheet(this.state.isRTL);
		const {navigate} = this.props.navigation;
    return (
      <View style={styles.container} >
      
          <View style={styles.containerCenter}>
          <View style={{flex: 0, flexDirection: 'row',borderBottomWidth: 1,borderBottomColor: '#f0f0f0',paddingTop:20,paddingBottom:20}}>
            <View style={{width: "65%"}}>

            </View>
            <View style={{width: "35%"}}>
                <View style={{flex: 0, flexDirection: 'row',justifyContent:'flex-end',padding:0}}>
			
			
				
                  
				<View style={{flex:0,width: "20%",justifyContent:'flex-end'}}>
                  <Text style={styles.textRotat}>Class</Text>
				  </View>
				  <View style={{flex:0,width: "20%",justifyContent:'flex-end'}}>
                   <Text style={styles.textRotat}>Division</Text>
                 </View>
                  <View style={{flex:0,width: "20%",justifyContent:'flex-end'}}>
                      <Text style={styles.textRotat}>Accept</Text>
				  </View>  
				   <View style={{flex:0,width: "20%",justifyContent:'flex-end',display:'flex',marginRight:5}}>
					  <Text style={styles.textRotat}>Reject</Text>
					</View> 
                
                </View>
            </View>
          </View>
          <View style={{flex: 0, flexDirection: 'row',borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
            <View style={{width: "75%"}}>
                   <View style={{flex: 0, flexDirection: 'row',padding:6}}>
                      <View style={styles.ColSet20}>
                          <Image style={{height:30,width:30,}} source={Person}/>
                       </View>
                      <View style={styles.ColSet80}>
                          <Text style={styles.fontSizeText}>Mohamed Ahmed Ali</Text>
                       </View>
                   </View>
                   <View style={{flex: 0, flexDirection: 'row',padding:6}}>
                      <View style={styles.ColSet20}>
                          <Image style={{height:30,width:30,}} source={confirmStaffIcon}/>
                       </View>
                      <View style={styles.ColSet80}>
                          <Text style={styles.fontSizeText}>Mr. Ali</Text>
                       </View>
                   </View>
                   <View style={{flex: 0, flexDirection: 'row',padding:6,marginLeft:20}}>
                      <View style={styles.ColSet20}>
                          <Image style={{height:30,width:30,}} source={confirmMsgIcon}/>
                       </View>
                      <View style={styles.ColSet80}>
                          <Text style={styles.fontSizeText}>Message.........</Text>
                       </View>
                   </View> 
                 </View>
                 <View style={styles.boxCenterDiv}>
                      <View style={{flex: 0, flexDirection: 'row'}}>
                        <View style={{width: "25%"}}>
                           <Text style={styles.BorderBox}>1</Text>
                        </View>
                        <View style={{width: "25%"}}>
                            <Text style={styles.BorderBox}>2</Text>
                        </View>
                        <View style={{width: "25%"}}>
                            <Image style={{height:22,marginTop:1}} source={cross}/>
                        </View>
						<View style={{width: "25%"}}>
                            <Image style={{height:22,marginTop:1}} source={cross}/>
                        </View>
                      </View>
                </View>
             </View> 

             <View style={{flex: 0, flexDirection: 'row',borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
            <View style={{width: "75%"}}>
                   <View style={{flex: 0, flexDirection: 'row',padding:6}}>
                      <View style={styles.ColSet20}>
                          <Image style={{height:30,width:30,}} source={Person}/>
                       </View>
                      <View style={styles.ColSet80}>
                          <Text style={styles.fontSizeText}>Ahmed Eid Mohamed</Text>
                       </View>
                   </View>
                   <View style={{flex: 0, flexDirection: 'row',padding:6}}>
                      <View style={styles.ColSet20}>
                          <Image style={{height:30,width:30,}} source={confirmStaffIcon}/>
                       </View>
                      <View style={styles.ColSet80}>
                          <Text style={styles.fontSizeText}>Mr. Abdul Tawab</Text>
                       </View>
                   </View>
                   <View style={{flex: 0, flexDirection: 'row',padding:6,marginLeft:20}}>
                      <View style={styles.ColSet20}>
                          <Image style={{height:30,width:30,}} source={confirmMsgIcon}/>
                       </View>
                      <View style={styles.ColSet80}>
                          <Text style={styles.fontSizeText}>Message .....</Text>
                       </View>
                   </View> 
                 </View>
                 <View style={styles.boxCenterDiv}>
                      <View style={{flex: 0, flexDirection: 'row'}}>
                       <View style={{width: "25%"}}>
                           <Text style={styles.BorderBox}>1</Text>
                        </View>
                        <View style={{width: "25%"}}>
                            <Text style={styles.BorderBox}>2</Text>
                        </View>
                        <View style={{width: "25%"}}>
                            <Image style={{height:22,marginTop:1}} source={cross}/>
                        </View>
						<View style={{width: "25%"}}>
                            <Image style={{height:22,marginTop:1}} source={cross}/>
                        </View>
                      </View>
                </View>
             </View> 

        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
	
  return {
   locale: state.locale,

  }
}

 

export default connect(mapStateToProps)(ConfirmExcuse)
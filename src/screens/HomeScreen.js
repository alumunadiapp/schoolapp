import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,ScrollView} from 'react-native';
import styles from '../styles/LoginStyles';
import Staff from '../imagesicon/home2_07.png';
import AbsentOne from '../imagesicon/home2_10.png';
import LateOne from '../imagesicon/home2_13.png';
import AbsentSchool from '../imagesicon/home2_15.png';
import LateArrival from '../imagesicon/home2_17.png';
import Disrupt from '../imagesicon/home2_18.png';
import Weak from '../imagesicon/home2_20.png';
import Behavioral from '../imagesicon/home2_21.png';
import Positive from '../imagesicon/home2_23.png';
import Free from '../imagesicon/home2_25.png';
import Apply from '../imagesicon/home2_27.png';
import Confirm from '../imagesicon/home2_29.png';
import ViewExcuse from '../imagesicon/home2_31.png';
import ViewToday from '../imagesicon/home2_33.png';

export default class HomeScreen extends React.Component {
   

  render() {
    return (
      <View style={styles.container} >
        <View style={styles.headerTops} >
          <Text style={{textAlign:'center',color:'white'}}>Home Screen</Text>
        </View>
          
          <View style={styles.containerCenter}>
          <ScrollView>
            <View  style={styles.imageCenter}>
                <Image style={{height:48,width:48}} source={Staff}/>
              </View>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={AbsentOne}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Apsent From One Period</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={LateOne}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Late From One Period</Text>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={AbsentSchool}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Apsent From School All Day</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={LateArrival}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Late Arrival to School</Text>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Disrupt}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Disrupt The Lession</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Weak}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Weak Academic Achievement</Text>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Behavioral}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Behavioral Violation</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Positive}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Positive Message</Text>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Free}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Free Message</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Apply}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Apply Excuses</Text>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Confirm}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Confirm Excuses</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={ViewExcuse}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>View Excuses</Text>
                 </View>
             </View>

             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={ViewToday}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>View Today's Behaviors</Text>
                 </View>
             </View>


             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                         
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                        <Text style={styles.fontSizeText}>Event Calendar</Text>
                 </View>  
             </View>

             </ScrollView>
             
        </View>

      </View>
    );
  }
}


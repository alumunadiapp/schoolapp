import React, { Component } from "react";
import { StyleSheet ,Button,Icon,View,TouchableOpacity,Image,I18nManager} from "react-native";
import LoginScreen from './LoginScreen.js';
import StaffScreen from './StaffScreen/StaffScreen.js';
import MessageScreen from './MessageScreen.js';
import MessageDetails from './MessageDetails.js';
import StudentFilter from './StudentFilter.js';
import SendBehaviour from './SendBehaviour.js';
import ViewExecuse from './ViewExecuse.js';
import ConfirmExcuse from './ConfirmExcuse.js';
import LanguageScreen from './LanguageScreen.js';
import NotificationScreen from './NotificationScreen.js';
import SoundScreen from './SoundSetting.js';
import DeleteStudent from './DeleteStudent.js';
import TeacherScreen from './TeacherScreen/TeacherScreen.js';
import SideMenuI from '../images/menu-lines.png';
import LeftMenuScreen from './LeftMenuScreen.js';
import RightMenuScreen from './RightMenuScreen.js';
import styles from '../styles/LoginStyles';
import I18n  from '../i18n.js';

//import MainScreenNavigator from "../ChatScreen/index.js";
//import Profile from "../ProfileScreen/index.js";
//import SideBar from "../SideBar/SideBar.js";
import { createStackNavigator,createDrawerNavigator, createAppContainer } from "react-navigation";

const DrawerButton = (props) => {
 
	return (
    <View style={{marginLeft:6}}>
      <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
        <Image
          source={SideMenuI}
        
        />
      </TouchableOpacity>
    </View>
  );
};

const RightDrawerButton = (props) => {
	console.log("Rigtht...",props);
	
	return (
    <View style={{marginRight:6}}>
      <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
        <Image
          source={SideMenuI}
          
        />
      </TouchableOpacity>
    </View>
  );
};


const AppNavigator = createStackNavigator(
	
  {
    Login:  LoginScreen,
    Staff:  StaffScreen,
	Teacher:TeacherScreen,
	StudentFilter:StudentFilter,
	SendBehaviour:SendBehaviour,
	MessageScreen:MessageScreen,
	MessageDetails:MessageDetails,
	ViewExecuse:ViewExecuse,
	ConfirmExcuse:ConfirmExcuse,
  },
  {
    initialRouteName: "Login",
	 defaultNavigationOptions:({navigation})=> ({
	
		title: I18n.t('School App'),
		
      headerStyle: {
        backgroundColor: '#5f021f',
		height:40
		
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
           textAlign: 'center',
		   flex:1 
        },
	  headerLeft:  <DrawerButton  navigation={navigation} />,
	  headerRight:  <RightDrawerButton navigation={navigation}  />
      
	
     }),
	 /**  Login: {
    screen:LoginScreen,
    navigationOptions: ({navigation}) => ({
      title: "Category List",
      headerLeft: <DrawerButton navigation={navigation}  />
    }),
  }**/

  },
  
); 

const  NotificationStack = createStackNavigator(
  {
    NotificationSetting: NotificationScreen,
  },
  {
	 defaultNavigationOptions:({navigation})=> ({
		title: 'Notification Settings',
      headerStyle: {
        backgroundColor: '#5f021f',
		height:40,
		
		
      },
	
      headerTintColor: '#fff',
      headerTitleStyle: {
			textAlign: 'center',
		   flex:1 
        },
	  headerLeft: <DrawerButton navigation={navigation}  />,
	  headerRight: <DrawerButton navigation={navigation}  />
     }),
	
  }
 
); 

const  LanguageStack = createStackNavigator(
  {
    LanguageSetting: LanguageScreen,
  },
  {
	 defaultNavigationOptions:({navigation})=> ({
		title: 'Language Settings',
      headerStyle: {
        backgroundColor: '#5f021f',
		height:40
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
           textAlign: 'center',
		   flex:1
        },
	  headerLeft: <DrawerButton navigation={navigation}  />
     }),
	
  }
 
); 

const  SoundStack = createStackNavigator(
  {
    SoundSetting: SoundScreen,
  },
  {
	 defaultNavigationOptions:({navigation})=> ({
		title: 'Sound Settings',
      headerStyle: {
        backgroundColor: '#5f021f',
		height:40
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
           textAlign: 'center',
		   flex:1
        },
	  headerLeft: <DrawerButton navigation={navigation}  />
     }),
	
  }
 
); 

const  DeleteStudentStack = createStackNavigator(
  {
    DeleteStudent :DeleteStudent ,
  },
  {
	 defaultNavigationOptions:({navigation})=> ({
		title: 'Delete Student ',
      headerStyle: {
        backgroundColor: '#5f021f',
		height:40
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
           textAlign: 'center',
		   flex:1
        },
	  headerLeft: <DrawerButton navigation={navigation}  />
     }),
	
  }
 
); 

 const RightDrawerApp = createDrawerNavigator({
    NotificationSetings:{
    screen:  NotificationStack,
  },
	
}, {
	
    drawerPosition: 'right',
    contentComponent: props=> <RightMenuScreen {...props} /> ,
    drawerOpenRoute: 'DrawerRightOpen',
    drawerCloseRoute: 'DrawerRightClose',
    
},
 

);



const MyDrawerNavigator = createDrawerNavigator({

  
   	Main:{
	screen:AppNavigator,  
   },
	RightDrawerApp:{
	   
	 screen:RightDrawerApp,  
   },
  LanguageSetings:{
    screen: LanguageStack,
  },
  NotificationSetings:{
    screen:  NotificationStack,
  },
   SoundSetings:{
    screen: SoundStack,
  },
   DeleteStudent:{
    screen: DeleteStudentStack,
  },
},
{
	  contentComponent:props=> <LeftMenuScreen {...props}/>,
	   drawerPosition: 'left',
	    drawerOpenRoute: 'LeftSideMenu',
		drawerCloseRoute: 'LeftSideMenuClose',
		drawerToggleRoute: 'LeftSideMenuToggle',
	  
},

 
 );

 

export default createAppContainer(MyDrawerNavigator );	


import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,TouchableHighlight,Picker} from 'react-native';
import { CheckBox } from 'react-native-elements'
//import styles from '../../styles/LoginStyles';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 

import I18n  from '../i18n.js';
import { connect } from 'react-redux';
class SendBehaviour extends React.Component {
	constructor(props) {
      super(props);
	 //console.log("dsdsdsdsd",I18nManager.isRTL);
	let isRTL= this.props.locale.locale=="ar" ? true : false ;
	this.state = { isRTL: isRTL,
				  checked:true

	};
	
	 I18n.locale = this.state.locale;
    }

	 componentDidMount() {
            this.props.navigation.setParams({});
               
        }	
	static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('School App'),
           
            };
        };
   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }

  render() {
	    const { navigate } = this.props.navigation;
		styles = StyleSheetFactory.getSheet(this.state.isRTL);	
    return (
      <View style={styles.container} >
      
          <View style={styles.containerCenter}>
			  <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center'}}>
				<View style={{borderBottomWidth:1,height: 30, width:250}}>
				 <Picker
					  selectedValue={this.state.locale}
					  style={{height: 30, width:250 }}
					  onValueChange={(itemValue, itemIndex) =>
						this.setState({language: itemValue})
					  }>
					  <Picker.Item label="Period" value="Period" />
					  <Picker.Item label="JavaScript" value="js" />
					</Picker>
				</View>
			</View>
			
			 <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center',}}>
				<View style={{borderBottomWidth:1,height: 30, width:250}}>
				 <Picker
					  selectedValue={this.state.locale}
					  style={{height: 30, width:250 }}
					  onValueChange={(itemValue, itemIndex) =>
						this.setState({language: itemValue})
					  }>
					  <Picker.Item label="Subject" value="Subject" />
					  <Picker.Item label="JavaScript" value="js" />
					</Picker>
				</View>
			</View>
			 <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center'}}>
				<View style={{borderWidth: 1, width:250}}>
					  <TextInput style={{paddingTop:10,borderTopWidth:0}}
					    multiline={true}
						numberOfLines={4}
                    onChangeText={(text) => this.setState({text})}
                    placeholder="Message"/>
				</View>
			</View>
			
			 <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center',marginTop:5}}>
				<View>
					 <Button color="#5f021f" title="Send" onPress={this.addUser}/>
				</View>
			</View>
			
             
        </View>
		
      </View>
     );
  }
}
const mapStateToProps = (state) => {
	
  return {
   locale: state.locale,

  }
}

 

export default connect(mapStateToProps)( SendBehaviour)

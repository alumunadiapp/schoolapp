import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,TouchableHighlight,Picker} from 'react-native';
import { CheckBox } from 'react-native-elements'
//import styles from '../../styles/LoginStyles';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
import LeftArrow from '../imagesicon/left-arrow-line.png';
import RightArrow from '../imagesicon/right-arrow-line.png';
import I18n  from '../i18n.js';
import { connect } from 'react-redux';
class StudentFilter extends React.Component {
	constructor(props) {
      super(props);
	 //console.log("dsdsdsdsd",I18nManager.isRTL);
	let isRTL= this.props.locale.locale=="ar" ? true : false ;
	this.state = { isRTL: isRTL,
				  checked:true

	};
	
	 I18n.locale = this.state.locale;
    }

	 componentDidMount() {
            this.props.navigation.setParams({});
               
        }	
	static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('School App'),
           
            };
        };
   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }

  render() {
	    const { navigate } = this.props.navigation;
		styles = StyleSheetFactory.getSheet(this.state.isRTL);	
    return (
      <View style={styles.container} >
      
          <View style={styles.containerCenter}>
             
			 <View style={styles.filterHeader}>
				 <View style={styles.filterHeaderTop1}>
					<View>
					  <Image style={styles.filtericonSize} source={LeftArrow }/>
					 </View>
					 <View>
					 <Text style={styles.filterHeadText}>Back</Text>
					 </View>
                 </View>
				  <View style={styles.filterHeaderTop2}>
					 <Text style={styles.filterHeadText}>Count</Text>
                 </View>
				  <View style={styles.filterHeaderTop3}>
					 <View>
					 <Text style={styles.filterHeadText} onPress={()=>navigate('SendBehaviour')}>Next</Text>
					 </View>
					  <View>
					  <Image style={styles.filtericonSize} source={RightArrow }/>
					 </View>
					 
                 </View>
			 
			 
			 </View>
			 
			  <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center'}}>
				<View style={{borderBottomWidth:1,height: 30, width:250}}>
				 <Picker
					  selectedValue={this.state.locale}
					  style={{height: 30, width:250 }}
					  onValueChange={(itemValue, itemIndex) =>
						this.setState({language: itemValue})
					  }>
					  <Picker.Item label="Class" value="class" />
					  <Picker.Item label="JavaScript" value="js" />
					</Picker>
				</View>
			</View>
			
			 <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center',}}>
				<View style={{borderBottomWidth:1,height: 30, width:250}}>
				 <Picker
					  selectedValue={this.state.locale}
					  style={{height: 30, width:250 }}
					  onValueChange={(itemValue, itemIndex) =>
						this.setState({language: itemValue})
					  }>
					  <Picker.Item label="Division" value="div" />
					  <Picker.Item label="JavaScript" value="js" />
					</Picker>
				</View>
			</View>
			 <View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',justifyContent:'center'}}>
				<View style={{borderBottomWidth:1,height: 30, width:250}}>
					  <TextInput style={{height:40,paddingTop:10}}
                    onChangeText={(text) => this.setState({text})}
                    placeholder="Student Name"/>
				</View>
			</View>


			<View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',width:'100%'}}>
				<View style={{width:'100%',height:30}}>
					<CheckBox
					  title='Student 1'
					  checked={this.state.checked}
					/>
				</View>
			
			</View>
			
			<View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',width:'100%'}}>
				<View style={{width:'100%',height:30}}>
					<CheckBox
					  title='Student 2'
					  checked={this.state.checked}
					/>
				</View>
			
			</View>
			
			<View style={{flex: 0,  flexDirection:  'row', borderColor: 'gray',width:'100%'}}>
				<View style={{width:'100%',height:30}}>
					<CheckBox
					  title='Student 3'
					  checked={this.state.checked}
					/>
				</View>
			
			</View>
			
             
        </View>
		
      </View>
     );
  }
}
const mapStateToProps = (state) => {
	
  return {
   locale: state.locale,

  }
}

 

export default connect(mapStateToProps)( StudentFilter)

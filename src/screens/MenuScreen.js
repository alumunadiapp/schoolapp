import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image} from 'react-native';
import styles from '../styles/LoginStyles';
import TimeTable from '../imagesicon/menu2_03.png';
import Academic from '../imagesicon/menu2_06.png';
import Sound from '../imagesicon/setting_08.png';
export default class MenuScreen extends React.Component {
   

  render() {
    return (
      <View style={styles.container} >
        <View style={styles.headerTops} >
          <Text style={{fontSize:20,color:'white'}}>Menu</Text>
        </View>
          
          <View style={styles.containerCenter}>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={TimeTable}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Time Table</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Academic}/>
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                    <Text style={styles.fontSizeText}>Academic Calendar</Text>
                 </View>
             </View>


             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                         
                     </View>
                 </View>
                <View style={styles.ColSet85}>
                        <Text style={styles.fontSizeText}>Event Calendar</Text>
                 </View>  
             </View>
             
        </View>

      </View>
    );
  }
}


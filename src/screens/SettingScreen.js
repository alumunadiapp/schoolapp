import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image} from 'react-native';
import styles from '../styles/LoginStyles';
import Language from '../imagesicon/setting_03.png';
import Notification from '../imagesicon/setting_06.png';
import Sound from '../imagesicon/setting_08.png';
import Delete from '../imagesicon/setting_11.png';
import Share from '../imagesicon/setting_14.png';
import Update from '../imagesicon/setting_17.png';
import Logout from '../imagesicon/setting_19.png';
export default class SettingScreen extends React.Component {
   

  render() {
    return (
      <View style={styles.container} >
        <View style={styles.headerTops} >
          <Text style={{fontSize:20,color:'white'}}>Settings</Text>
        </View>
          
          <View style={styles.containerCenter}>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Language}/>
                     </View>
                 </View>
                <View style={styles.ColSet90}>
                    <Text style={styles.fontSizeText}>Language Setting</Text>
                 </View>                 
                 
             </View>
             
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Notification}/>
                     </View>
                 </View>
                <View style={styles.ColSet90}>
                    <Text style={styles.fontSizeText}>Notification Setting</Text>
                 </View>                 
                 
             </View>


             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                         <Image style={{height:30,width:30,}} source={Sound}/>
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                        <Text style={styles.fontSizeText}>Sound Setting</Text>
                    
                 </View>                 
                 
             </View>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Delete}/>
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                    <Text style={styles.fontSizeText}>Delete Setting</Text>
                 </View>
             </View>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Share}/>
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                    <Text style={styles.fontSizeText}>Share App</Text>
                 </View>
             </View>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Update}/>
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                    <Text style={styles.fontSizeText}>Update</Text>
                 </View>
             </View>
             <View style={{flex: 0, flexDirection: 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
                <View style={styles.ColSet15}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Logout}/>
                     </View>
                 </View>
                <View style={styles.ColSet80}>
                    <Text style={styles.fontSizeText}>Logout</Text>
                 </View>
             </View>
        </View>

      </View>
    );
  }
}


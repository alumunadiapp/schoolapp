import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,Switch,TouchableOpacity} from 'react-native';
//import styles from '../styles/LoginStyles';
import Delete from '../imagesicon/setting_11.png';
import { connect } from 'react-redux';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
import SideMenuI from '../images/menu-lines.png';
import I18n  from '../i18n.js';
 class SoundSetting extends React.Component {
   
   constructor(props) {
      super(props);
      this.state = { SwitchValue: false };
    }
		static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('Sound Settings'),
			 header: null,
			  headerStyle: {
				  elevation: 0,
				  shadowOpacity: 0,
				  borderBottomWidth: 0,
				  top:0
				}
            };
        };

  render() {
	  I18n.locale = this.props.locale.locale;
	  styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
    return (
      <View style={styles.container} >
        <View style={styles.headerTops} >
            <View style={{flex: 0, flexDirection: 'row'}}>
				<View>
				  <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
					<Image
					  source={SideMenuI}
					  style={styles.icon}
					/>
				  </TouchableOpacity>
				</View>
			
                <View style={styles.ColSet80}>
                    <View style = {styles.textAlignDiv} >
                   <Text style={{fontSize:20,color:'white'}}>{ I18n.t('Sound Settings')}</Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet20}>
                    <View style = {styles.boxCenterDiv}>
                         <TouchableOpacity onPress={this.showCalculation}>
                            <Text style={{color:'white',fontSize:18}}> { I18n.t('Done')} </Text>
                        </TouchableOpacity>
                     </View>
                 </View>
				 
             </View>
          
        </View>
          
          <View style={styles. containerCenter}>
             <View style={{flex: 0, flexDirection: 'row',padding:80,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                <View style={styles.ColSet80}>
                    <View style = {styles.textAlignDiv} >
                    <Text style={{color:'black',fontSize:18}}>{ I18n.t('Message Notification')} </Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet20}>
                    <View style = {styles.boxCenterDiv} >
                        <Switch 
                          onValueChange={(value) => this.setState({SwitchValue: value})}
                          value={this.state.SwitchValue} />
                     </View>
                 </View>
             </View>
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
	
	return {
   locale: state.locale,
    isRTL: state.isRTL,

  }
}
 
  const mapDispatchToProps = dispatch => ({
    setLocale: (locale) => dispatch(setLocale(locale))
  })
 

export default connect(mapStateToProps, mapDispatchToProps)(SoundSetting)
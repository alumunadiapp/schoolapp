import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,ScrollView} from 'react-native';
import styles from '../styles/LoginStyles';
import Logo from '../images/newstudent.png';
import Mail from '../images/mail.png';
import Call from '../images/phone-green.png';
import Parent from '../images/Parent-icon.png';
import Staff from '../images/man-icon-white.png';
import Degree from '../images/graduation.png';
import Lock from '../images/lock.png';
import LanguageIcon from '../imagesicon/setting_03.png';
import NotifiactionIcon from '../imagesicon/setting_06.png';
import SoundIcon from '../imagesicon/setting_08.png';
import DeleteIcon from '../imagesicon/setting_11.png';
import ShareIcon from '../imagesicon/setting_14.png';
import UpdateIcon from '../imagesicon/setting_17.png';
import LogoutIcon from '../imagesicon/setting_19.png';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
export default class RightMenuScreen extends React.Component {


   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }
	
	navigateToScreen = (route) => () => {
		console.log("dsdsds",route);
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }


 render () {
    return (
      <View style={styles.leftMenuContainer}>
        <ScrollView>
          <View>
            <Text style={styles.sectionHeadingStyle}>
              Settings
            </Text>
            <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={LanguageIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('TimeTable')}>
				Time Table
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={NotifiactionIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('AcadamicCalndar')}>
				Academic Calendar
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={SoundIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('SoundSetings')}>
				Sound Settings
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={DeleteIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('DeleteStudent')}>
				Delete Student
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={ShareIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Share App')}>
				Share App
              </Text>
            </View>
			 <View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={UpdateIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LanguageSetings')}>
				Update
              </Text>
            </View>
			<View style={styles.navSectionStyle}>
			  <Image style={styles.navMenuIcon} source={LogoutIcon}/>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LanguageSetings')}>
				Logout
              </Text>
            </View>
			
          </View>
       
        </ScrollView>
       
      </View>
    );
  }
}

RightMenuScreen.propTypes = {
  navigation: PropTypes.object
};



import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,TouchableHighlight} from 'react-native';
//import styles from '../../styles/LoginStyles';
import Logo from '../../images/newstudent.png';
import Mail from '../../images/mail.png';
import Call from '../../images/phone-green.png';
import Parent from '../../images/Parent-icon.png';
import Teacher from '../../imagesicon/home2_38.png';
import TeacherIcon from '../../imagesicon/home2_07.png';
import Staff from '../../imagesicon/staff_32.png';
import Degree from '../../images/graduation.png';
import Lock from '../../images/lock.png';
import AbsentPIcon from '../../imagesicon/home2_10.png';
import LatePIcon from '../../imagesicon/home2_13.png';
import RightArrow from '../../imagesicon/arrow-right.png';
import DisruptIcon from '../../imagesicon/home2_18.png';
import WeekAcadamicIcon from '../../imagesicon/home2_20.png';
import BehaviourIcon from '../../imagesicon/home2_21.png';
import PositiveMsgIcon from '../../imagesicon/home2_23.png';
import StyleSheetFactory from '../../styles/Ar_loginStyles.js'; 
import FreeMsgIcon from '../../imagesicon/home2_25.png';
import LeftArrow from '../../imagesicon/left-arrow.png';
import I18n  from '../../i18n.js';
import { connect } from 'react-redux';
class TeacherScreen extends React.Component {
	constructor(props) {
      super(props);
	 //console.log("dsdsdsdsd",I18nManager.isRTL);
	let isRTL= this.props.locale.locale=="ar" ? true : false ;
	this.state = { isRTL: isRTL };
	 I18n.locale = this.state.locale;
    }

	 componentDidMount() {
            this.props.navigation.setParams({});
               
        }	
	static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('School App'),
           
            };
        };
   addUser = () => {
       this.setState(prevState=>{
        return{
            text:'',
            users:[...prevState.users,prevState.text]
        } 
       })
    }

  render() {
	    const { navigate } = this.props.navigation;
		styles = StyleSheetFactory.getSheet(this.state.isRTL);	
    return (
      <View style={styles.container} >
      
          <View style={styles.containerCenter}>
              <View  style={styles.imageCenter}>
                <Image style={styles.imageLogo} source={TeacherIcon}/>
              </View>
        
		
			  
			  <View style={{flex: 0,  flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={AbsentPIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Absent From One Period')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			  
			  	 <View style={{flex: 0,  flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={LatePIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Late From One Period')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			
			 
			   <View style={{flex: 0,  flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={DisruptIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Dirupt The Lesson')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			  
			   <View style={{flex: 0,  flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={WeekAcadamicIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Weak Academic Achievement')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			  
			   <View style={{flex: 0, flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={BehaviourIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Behavioural Violation')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			  
			   <View style={{flex: 0,  flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={PositiveMsgIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Positive Message')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			  
			   <View style={{flex: 0,  flexDirection: this.state.isRTL ? 'row-reverse': 'row', borderColor: 'gray',borderBottomWidth: 0.5}}>
                 <View style={styles.LogininputIcon}>
                    <Image style={styles.iconSize} source={FreeMsgIcon}/>
                 </View>
                 <View>
					 <Text style={styles.staffMenuName}>{I18n.t('Free Message')}</Text>
                 </View>
				  <View style={styles.staffMenuRightArrow}>
                    <Image style={styles.rightArrowIconSize} source={this.state.isRTL ? LeftArrow : RightArrow}/>
                 </View>
              </View>
			  
			  
			  
			 
             
        </View>
		<View style={styles.FooterBottom} >
            <View style={{flex: 1, flexDirection: 'row'}}>
               <View style={styles.ColSet33}>

                 <View style={styles.FlexCenter} >
                  <Image
                    style={styles.FooterImage}
                    source={Parent}
                  />
				  <TouchableHighlight onPress={()=>navigate('Login')}>
                  <Text style={{textAlign:'center', 
                  color:'white',}}>{I18n.t('Parent')}</Text>
				   </TouchableHighlight>
                  </View>
               </View>

              <View style={styles.ColSet33}>
               
                  <View style={styles.FlexCenter} >
				   <TouchableHighlight onPress={()=>navigate('Staff')}>
                  <Image
                    style={styles.FooterImageStaff}
                    source={Staff}
                  />
				   </TouchableHighlight>
				  <TouchableHighlight onPress={()=>navigate('Staff')}>
                  <Text style={{textAlign:'center',color:'white'}} >{I18n.t('Staff')}</Text>
				  </TouchableHighlight>
                  </View>
              </View>
			  <View style={styles.ColSet33}>

                  <View style={styles.FlexCenter} >
				   <TouchableHighlight onPress={()=>navigate('Teacher')}>
                  <Image
                    style={styles.FooterImageStaff}
                    source={Teacher}
                  />
				    </TouchableHighlight>
				  <TouchableHighlight onPress={()=>navigate('Teacher')}>
                  <Text style={{textAlign:'center',color:'white'}} onPress={()=>navigate('Teacher')}>{I18n.t('Teacher')}</Text>
				  </TouchableHighlight>
                  </View>
              </View>
		</View>
        </View>
      </View>
     );
  }
}
const mapStateToProps = (state) => {
	
  return {
   locale: state.locale,

  }
}

 

export default connect(mapStateToProps)( TeacherScreen)

import React from 'react';
import {
   Text, View, TextInput, Button, Image, TouchableHighlight,
   Keyboard
} from 'react-native';
import Logo from '../images/newstudent.png';
import Mail from '../imagesicon/message_14.png';
import Call from '../imagesicon/loginscreen_12.png';
import Parent from '../imagesicon/home2_36.png';
import Teacher from '../imagesicon/home2_38.png';
import Staff from '../imagesicon/staff_32.png';
import Degree from '../imagesicon/loginscreen_03.png';
import Lock from '../imagesicon/loginscreen_06.png';
import I18n from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
//import styles from '../styles/LoginStyles';
import { connect } from 'react-redux';
import { addStudent } from '../actions/actions';
import { getStudentLocal } from '../actions/actions';
import StudentListComp from '../components/login/StudentListComp'
import { AsyncStorage } from 'react-native'

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    let isRTL = this.props.locale.locale == "ar" ? true : false;
    this.state = { isRTL: isRTL };
    I18n.locale = this.state.locale;
  }

  componentDidMount() {
    this.props.navigation.setParams({});
    this.props.getStudentLocal()

  }
  static navigationOptions = ({ navigation }) => {

    return {
      headerTitle: I18n.t('School App'),

    };
  };

  addStudent = () => {
    Keyboard.dismiss()
    this.props.addStudent(this.props.locale.schoolId,this.props.locale.password)
  }
  clearAsyncStorage = async() => {
    console.log("clearing storage")
    AsyncStorage.clear();
    
  }
  render() {
    if (this.state.isRTL) {
      styles = StyleSheetFactory.getSheet(this.state.isRTL);
    }
    styles = StyleSheetFactory.getSheet(this.state.isRTL);
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container} >
        <View style={styles.containerCenter}>
          <View style={styles.imageCenter}>
            <Image style={styles.imageLogo} source={Logo} />
          </View>
          <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', borderColor: 'gray', borderWidth: 1, width: '90%', marginLeft: '5%', marginRight: '5%' }}>
            <View style={styles.LogininputIcon}>
              <Image style={styles.iconSize} source={Degree} />
            </View>
            <View >
              <TextInput style={styles.inputSet}
                onChangeText={(text) => this.props.locale.schoolId = text}
                // value={this.props.locale.schoolId}
                placeholder={I18n.t('School ID')} />
            </View>
          </View>
          <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', borderColor: 'gray', borderWidth: 1, marginBottom: 10, width: '90%', marginLeft: '5%', marginRight: '5%' }}>
            <View style={styles.LogininputIcon}>
              <Image style={styles.iconSize} source={Lock} />
            </View>
            <View>
              <TextInput style={styles.inputSet}
                onChangeText={(text) => this.props.locale.password = text}
                // value={this.props.locale.password}
                placeholder={I18n.t('Password')} />
            </View>
          </View>

          <View style={{ margin: 10 }}>
            <Button color="#5f021f" title={I18n.t('Add Student')} 
            // onPress={() => this.props.addStudent}
            onPress={this.addStudent}/>
          </View>
          <View style={{ margin: 10 }}>
            <Button color="#5f021f" title='clear storage' onPress={this.clearAsyncStorage} />
          </View>
          {this.props.locale.students.map((student, key) => {
            if (student != null) {
              return <StudentListComp key={key} locale={this.props.locale.locale} 
              navigation={navigate} student={student}>
            </StudentListComp>
            }
            
          })}

        </View>

        <View style={styles.FooterBottom} >
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={styles.ColSet33}>

              <View style={styles.FlexCenter} >
                <Image
                  style={styles.FooterImage}
                  source={Parent}
                />
                <TouchableHighlight onPress={() => navigate('Login')}>
                  <Text style={{
                    textAlign: 'center',
                    color: 'white',
                  }}>{I18n.t('Parent')}</Text>
                </TouchableHighlight>
              </View>
            </View>

            <View style={styles.ColSet33}>

              <View style={styles.FlexCenter} >
                <TouchableHighlight onPress={() => navigate('Staff')}>
                  <Image
                    style={styles.FooterImageStaff}
                    source={Staff}
                  />
                </TouchableHighlight>
                <TouchableHighlight onPress={() => navigate('Staff')}>
                  <Text style={{ textAlign: 'center', color: 'white' }} >{I18n.t('Staff')}</Text>
                </TouchableHighlight>
              </View>
            </View>
            <View style={styles.ColSet33}>

              <View style={styles.FlexCenter} >
                <TouchableHighlight onPress={() => navigate('Teacher')}>
                  <Image
                    style={styles.FooterImageStaff}
                    source={Teacher}
                  />
                </TouchableHighlight>
                <TouchableHighlight onPress={() => navigate('Teacher')}>
                  <Text style={{ textAlign: 'center', color: 'white' }} onPress={() => navigate('Teacher')}>{I18n.t('Teacher')}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  addStudent: (schoolId,password) => dispatch(addStudent(schoolId,password)),
  getStudentLocal: () => dispatch(getStudentLocal())
})


const mapStateToProps = (state) => {
  const { login } = state
  // return { login }
  return {
    locale: state.locale,
    studentName: state.studentName

  }
}



export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
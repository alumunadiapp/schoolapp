import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image,CheckBox} from 'react-native';
//import styles from '../styles/LoginStyles';
import { RadioButton } from 'react-native-paper';
import { connect } from 'react-redux';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
import { setLocale } from '../actions/actions';
import I18n  from '../i18n.js';
 class LanguageScreen extends React.Component {
   state = {
    checked: 'ar',
	
  };
  
  	 constructor(props) {
      super(props);
	  
    }
	
	static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('Language Settings'),
           
            };
        };

  render() {
		I18n.locale = this.props.locale.locale;
	  	styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);

	  
	   const { checked } = this.state;
    return (
      <View style={styles.container} >
        
          
          <View style={styles.containerCenter}>
             <View style={{flex: 0, flexDirection: this.state.isRTL ? 'row-reverse': 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                
                <View style={styles.ColSet90}>
                  <View style={styles.textAlignDiv}>
                    <Text style={{color:'black',fontSize:18}}>{ I18n.t('English')} </Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet10}>
                   <RadioButton
					  value="eng"
					  status={checked === 'eng' ? 'checked' : 'unchecked'}
					  onPress={() => this.props.setLocale('en')}
					/>
                 </View>
             </View>
             
             <View style={{flex: 0, flexDirection: this.state.isRTL ? 'row-reverse': 'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
             
                <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                <Text style={{color:'black',fontSize:18}}>{ I18n.t('Arabic')}</Text>
                </View>
                 </View>                 
                 <View style={styles.ColSet10}>
                   <RadioButton
					  value="ar"
					  status={checked === 'ar' ? 'checked' : 'unchecked'}
					  onPress={() => this.props.setLocale('ar')}
					/>
                 </View>
             </View>
             <View  style={{margin:10,backgroundColor: '#911610'}}>
                <Button color="#5f021f" title={ I18n.t('SAVE')} onPress={this.addUser}/>
              </View>
        </View>

      </View>
    );
  }
}
const mapStateToProps = (state) => {
	
	return {
   locale: state.locale,
    isRTL: state.isRTL,

  }
}
 
  const mapDispatchToProps = dispatch => ({
    setLocale: (locale) => dispatch(setLocale(locale))
  })
 

export default connect(mapStateToProps, mapDispatchToProps)(LanguageScreen)

import React from 'react';
import {StyleSheet, Text, View,TextInput,Button,Image} from 'react-native';
//import styles from '../styles/LoginStyles';
import Delete from '../imagesicon/setting_11.png';
import StyleSheetFactory from '../styles/Ar_loginStyles.js'; 
import { connect } from 'react-redux';
import I18n  from '../i18n.js';
 class DeleteStudent extends React.Component {
	 
	 constructor(props){
		 
		 super(props);
		 let isRTL= this.props.locale.locale=="ar" ? true : false ;
		 this.state = { isRTL: isRTL };
		 I18n.locale = this.state.locale;
		 
		 
	 }
	 componentDidMount() {
            console.log("Delete ---",this.props);
               
        }	
   static navigationOptions = ({navigation}) => {
		
        return {
            headerTitle: I18n.t('Delete Student'),
           
            };
        };

  render() {
	   I18n.locale = this.props.locale.locale;
	  	styles = StyleSheetFactory.getSheet(this.state.isRTL);
		
    return (
      <View style={styles.container} >

          
          <View style={styles.containerCenter}>
             <View style={{flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' :'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0',}}>
                
                <View style={styles.ColSet80}>
                <View style={styles.textAlignDiv}>
                    <Text style={{color:'black',fontSize:18}}>Student1</Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet20}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Delete}/>
                     </View>
                 </View>
             </View>
             
             <View style={{flex: 0, flexDirection:this.state.isRTL? 'row-reverse' :'row',padding:12,borderBottomWidth: 1,borderBottomColor: '#f0f0f0'}}>
             
                <View style={styles.ColSet80}>
                <View style={styles.textAlignDiv}>
                    <Text style={{color:'black',fontSize:18}}>Student1</Text>
                    </View>
                 </View>                 
                 <View style={styles.ColSet20}>
                    <View style = {styles.FlexCenter} >
                     <Image style={{height:30,width:30,}} source={Delete}/>
                     </View>
                 </View>
             </View>

             
             
            
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
	
	return {
   locale: state.locale,
    isRTL: state.isRTL,

  }
}
 
  const mapDispatchToProps = dispatch => ({
    setLocale: (locale) => dispatch(setLocale(locale))
  })
 

export default connect(mapStateToProps, mapDispatchToProps)( DeleteStudent)
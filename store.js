import { createStore, combineReducers,applyMiddleware } from 'redux';
import appReducer from './src/reducers/reducer';
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'
const rootReducer = combineReducers({
  locale: appReducer
});

const configureStore = () => {
  return createStore(rootReducer,
    applyMiddleware(thunkMiddleware,logger));
}

export default configureStore;
import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, Image } from 'react-native';
import SchoolAppNavigator from './src/screens/SchoolAppNavigator.js';
import { connect } from 'react-redux';
console.log("in app")

class App extends React.Component {
    render() {
        return (
            <SchoolAppNavigator/>
        );
    }
}


const mapStateToProps = (state) => {
  //onsole.log("My App", state.locale );
  return {
    locale: state.locale,
	isRTL:state.isRTL
  }
  
}



export default connect(mapStateToProps)(App);

